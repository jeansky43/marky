use classicmodelsV2;

UPDATE classicmodels.offices SET country = 'United Kingdom' WHERE country = 'UK'; 
UPDATE classicmodels.offices SET country = 'United States' WHERE country = 'USA'; 
UPDATE classicmodels.offices SET country = 'Russian Federation' WHERE country = 'RUSSIA'; 
UPDATE classicmodels.offices SET country = 'Norway' WHERE country = 'Norway  '; 


UPDATE classicmodels.customers SET country = 'United Kingdom' WHERE country = 'UK';  
UPDATE classicmodels.customers SET country = 'United States' WHERE country = 'USA'; 
UPDATE classicmodels.customers SET country = 'Russian Federation' WHERE country = 'RUSSIA'; 
UPDATE classicmodels.customers SET country = 'Norway' WHERE country = 'Norway  ';


INSERT INTO classicmodelsV2.offices SELECT 
officeCode,
city,
phone,
addressLine1,
addressLine2,
state,
world.country.Code,
postalCode,
territory
FROM classicmodels.offices
JOIN world.country ON world.country.Name = classicmodels.offices.country; 

INSERT INTO classicmodelsV2.employees SELECT 
employeeNumber,
lastName,
firstName,
extension,
email,
officeCode,
reportsTo,
jobTitle
FROM classicmodels.employees;

INSERT INTO classicmodelsV2.customers(
customerNumber,
  customerName,
  contactLastName,
  contactFirstName,
  phone,
  addressLine1,
  addressLine2,
  city,
  state,
  postalCode,
  country,
  salesRepEmployeeNumber,
  creditLimit)
SELECT 
  customerNumber,
  customerName,
  contactLastName,
  contactFirstName,
  phone,
  addressLine1,
  addressLine2,
  city,
  state,
  postalCode,
  world.country.Code,
  salesRepEmployeeNumber,
  creditLimit
FROM classicmodels.customers
JOIN world.country ON world.country.Name = classicmodels.customers.country;

INSERT INTO classicmodelsV2.orders SELECT 
orderNumber,
orderDate,
requiredDate,
shippedDate,
status,
comments,
customerNumber
FROM classicmodels.orders;

INSERT INTO classicmodelsV2.payments SELECT 
*
FROM classicmodels.payments;

INSERT INTO classicmodelsV2.productlines SELECT 
*
FROM classicmodels.productlines;

INSERT INTO classicmodelsV2.products SELECT 
*
FROM classicmodels.products;

INSERT INTO classicmodelsV2.orderdetails SELECT 
*
FROM classicmodels.orderdetails;




