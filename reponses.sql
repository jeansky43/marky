use classicmodelsV2;

-- # 1 Pour chaque pays (avec au moins 1 client) le nombre de client, 
--la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.

SELECT
    c.country,
    COUNT(DISTINCT c.customerNumber) AS nombre_clients,
    MAX(c.Lang) AS langue_officielle,
    SUM(p.amount) AS chiffre_affaires_total
FROM
    customers c
JOIN
    payments p ON c.customerNumber = p.customerNumber
GROUP BY
    c.country
HAVING
    nombre_clients > 0;

-- # 2 La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

SELECT
    c.customerNumber,
    c.customerName,
    SUM(p.amount) AS chiffre_affaires_total
FROM
    customers c
JOIN
    payments p ON c.customerNumber = p.customerNumber
GROUP BY
    c.customerNumber, c.customerName
ORDER BY
    chiffre_affaires_total DESC
LIMIT
    10;

-- # 3 La durée moyenne (en jours) entre les dates de commandes et les dates 
--d’expédition (de la même commande).

SELECT
    orderNumber,
    AVG(DATEDIFF(shippedDate, orderDate)) AS duree_moyenne_expedition
FROM
    orders
WHERE
    shippedDate IS NOT NULL
GROUP BY
    orderNumber;

-- # 4 Les 10 produits les plus vendus.

SELECT
    orderdetails.productCode,
    products.productName,
    SUM(orderdetails.quantityOrdered) AS total_vendu
FROM
    orderdetails
JOIN
    products ON orderdetails.productCode = products.productCode
GROUP BY
    orderdetails.productCode, products.productName
ORDER BY
    total_vendu DESC
LIMIT 10;

-- # 5 Pour chaque pays le produits le plus vendu.

WITH RankedProducts AS (
    SELECT
        o.customerNumber,
        c.country,
        p.productCode,
        p.productName,
        od.quantityOrdered,
        ROW_NUMBER() OVER (PARTITION BY c.country ORDER BY od.quantityOrdered DESC) AS row_num
    FROM
        orders o
    JOIN
        orderdetails od ON o.orderNumber = od.orderNumber
    JOIN
        products p ON od.productCode = p.productCode
    JOIN
        customers c ON o.customerNumber = c.customerNumber
)
SELECT
    customerNumber,
    country,
    productCode,
    productName,
    quantityOrdered
FROM
    RankedProducts
WHERE
    row_num = 1;

-- # 6 Le produit qui a rapporté le plus de bénéfice.

SELECT
    p.productCode,
    p.productName,
    SUM((od.priceEach - p.buyPrice) * od.quantityOrdered) AS totalProfit
FROM
    products p
JOIN
    orderdetails od ON p.productCode = od.productCode
GROUP BY
    p.productCode, p.productName
ORDER BY
    totalProfit DESC
LIMIT 1;

-- # 7 La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).

SELECT
    AVG((p.MSRP - od.priceEach) / p.MSRP * 100) AS averagePriceDifferencePercentage
FROM
    products p
JOIN
    orderdetails od ON p.productCode = od.productCode;

-- # 8 Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.

SELECT
    c.country,
    COUNT(o.officeCode) AS numberOfOffices
FROM
    customers c
JOIN
    offices o ON c.country = o.country
GROUP BY
    c.country
HAVING
    COUNT(DISTINCT c.customerNumber) > 0;



