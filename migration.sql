USE classicmodelsV2;

ALTER TABLE customers
MODIFY COLUMN country CHAR(3) NOT NULL;

ALTER TABLE customers
ADD COLUMN Lang varchar(50) DEFAULT NULL;


ALTER TABLE offices
MODIFY COLUMN country CHAR(3) NOT NULL;


ALTER TABLE orders
MODIFY COLUMN status ENUM ('Shipped', 'Cancelled', 'Resolved', 'On Hold', 'Disputed', 'In Process') NOT NULL;


-----


