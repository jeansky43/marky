# Nom de la Base de données: **world**
# Le modèle physique des données
![Ci-joint le modèle physique, format png](<modèle physique.png>)

# Pour chaque type utilisé pour au moins un attribut : 
## le definir et expliquer pourquoi on a choisit ce type ?
1. **int**:
Un 'int' est un type de données qui représente un nombre entier en programmation. Le terme 'int' est une abréviation courante pour "integer" en anglais, et il est utilisé pour stocker des nombres entiers dans la plupart des langages de programmation.
Comparativement aux nombres à virgule flottante ('float'),on choisit les 'integers'parce qu'ils peuvent être plus efficaces en termes d'utilisation de la mémoire car ils n'ont pas de partie fractionnaire.
1. **char**:
Un 'char' est un type de données qui représente un caractère unique en programmation. Le terme 'char' est une abréviation de "character" en anglais, et il est utilisé pour stocker un seul caractère, tel qu'une lettre, un chiffre, un symbole ou un espace, dans la plupart des langages de programmation.
On a choisi ce type parce qu'il est utilisé pour stocker des chaînes de longueur fixe, il réserve une quantité fixe d'espace, il est également facile à récupérer contrairement au varchar.
1. **enum**:
Un 'enum' est un type de données qui permet de restreindre une colonne à un ensemble de valeurs prédéfinies. L'enum (énumération) permet de définir un ensemble de valeurs possibles pour une colonne. 
On a choisi ce type parce que la colonne 'Continent'réunit les différents continents comme valeurs, par conséquent ce type colle parfaitement au champs 'Continent'. L'avantage principal de l'utilisation d'un 'enum' est la limitation des valeurs autorisées pour une colonne spécifique, ce qui peut rendre le schéma de base de données plus restrictif et plus explicite
1. **décimal**:
Un 'décimal' est un type utilisé pour stocker des nombres décimaux à virgule fixe. Il est également connu sous le nom de NUMERIC. Ce type de données est utilisé pour stocker des nombres avec une partie fractionnaire précise, où la précision et l'échelle des chiffres après la virgule décimale sont spécifiées.
On a choisi ce type parce qu'on souhaiterait stocker des nombres avec une précision fixe,évitant les erreurs d'arrondi. On les a également choisi parce que les colonnes déclarées 'décimal'dans la base de données nous permettent de contrôler la quantité de stockage nécessaire en fonction des besoins.
1. **smallint**:
Un 'smallint' est un type de données utilisé pour stocker des nombres entiers relativement petits. C'est un type de données numérique qui occupe moins d'espace de stockage que d'autres types entiers plus larges comme 'int' ou 'bigint'. Il y a par conséquent une économie d'espace.
On a choisi ce type parce que la base de données dispose d'un champs "IndepYear" dans lequel seront stockées les années d'indépendance.

# une "explication" ou "description" des informations que l'on peut tirer de chaque colonne de chaque table.
Notre base de données est constituée de trois(3) tables:
***country***; ***city***; ***countrylanguage***.

## country : 
cette table contient plusieurs informations sur différents pays du monde.

***Nom du pays (Name)*** : Permet d'identifier le nom officiel de chaque pays.

***Code du pays (Code)*** : Un identifiant unique à trois lettres pour chaque pays.

***Continent (Continent)*** : Indique à quel continent appartient le pays (Asie, Europe, Amérique du Nord, Afrique, Océanie, Antarctique, Amérique du Sud).

***Région (Region)*** : Détaille la région géographique spécifique du pays.

***Superficie (SurfaceArea)*** : La taille totale en kilomètres carrés du pays.

***Année d'indépendance (IndepYear)*** : Si le pays est indépendant, cette colonne indique l'année d'obtention de son indépendance.

***Population (Population)*** : Le nombre d'habitants du pays.

***Espérance de vie (LifeExpectancy)*** : La durée de vie moyenne des habitants du pays.

***Produit national brut (GNP)*** : La mesure économique du produit national brut du pays.

***Ancien Produit national brut (GNPOld)*** : Mesure économique antérieure du produit national brut du pays.

***Nom local (LocalName)*** : Le nom du pays dans sa langue locale.

***Forme de gouvernement (GovernmentForm)*** : La manière dont le pays est gouverné.

***Chef d'État (HeadOfState)*** : Le dirigeant officiel du pays.

***Capitale (Capital)*** : L'identifiant de la ville qui est la capitale du pays.

***Code ISO 3166-1 alpha-2 (Code2)*** : Code à deux lettres conforme à la norme ISO 3166-1 pour le pays.

En utilisant ces informations, on peut effectuer diverses analyses, comparer des pays, afficher des statistiques démographiques ou économiques, etc. Ces données offrent une vue détaillée et variée des caractéristiques des pays répertoriés dans la table 'country'.

## city: 
cette table contient des informations spécifiques sur les villes du monde.

***ID de la ville (ID)*** : Un identifiant unique pour chaque ville (auto-incrémenté).

***Nom de la ville (Name)*** : Le nom de la ville.

***Code du pays (CountryCode)*** : Le code du pays auquel la ville appartient (clé étrangère faisant référence à 'country.Code').

***District (District)*** : La région ou le district où se trouve la ville.

***Population (Population)*** : Le nombre d'habitants de la ville.

Ces informations permettent de décrire et d'identifier spécifiquement chaque ville répertoriée dans la table 'city'. Elles sont utilisées pour associer chaque ville à son pays d'appartenance, spécifier sa localisation géographique, et donner une estimation de sa population.
En utilisant ces données, on peut effectuer des requêtes pour obtenir des statistiques sur la population urbaine, comparer la taille des villes, étudier la répartition géographique des villes par pays ou région, etc.

## countrylanguage:
cette table stocke des informations sur les langues parlées dans différents pays.

***Code du pays (CountryCode)*** : Identifie le pays auquel la langue appartient (clé étrangère faisant référence à 'country.Code').

***Langue (Language)*** : Le nom de la langue parlée dans le pays.

***Est officielle (IsOfficial)*** : Indique si la langue est une langue officielle dans le pays (T pour Vrai, F pour Faux).

***Pourcentage (Percentage)*** : Le pourcentage de la population du pays qui parle cette langue.

Ces informations permettent de comprendre les différentes langues parlées dans un pays donné, de savoir si elles ont un statut officiel, et d'estimer la proportion de la population parlant chaque langue spécifique.
En utilisant ces données, on peut réaliser des analyses linguistiques pour comprendre la diversité linguistique d'un pays, évaluer l'importance des langues officielles ou régionales, etc. Ces informations sont utiles pour explorer la diversité culturelle et linguistique à l'échelle mondiale.

# un liste, pour chaque table, des contraintes (pourquoi il y a ces contraintes ?)

## Table country :
***PRIMARY KEY (Code)*** : Cette contrainte indique que le champ 'Code' est la clé primaire de la table 'country'. Chaque pays a un code unique, ce qui garantit son identification distincte dans la table.

***FOREIGN KEY (Capital) REFERENCES city (ID)*** : La contrainte FOREIGN KEY lie la colonne 'Capital' à la colonne 'ID' de la table 'city'. Cela établit une relation entre la capitale d'un pays et la ville correspondante dans la table 'city'.

## Table city :
***PRIMARY KEY (ID)*** : Cette contrainte spécifie que le champ 'ID' est la clé primaire de la table 'city'. Chaque ville a un identifiant unique.

***FOREIGN KEY (CountryCode) REFERENCES country (Code)*** : La contrainte FOREIGN KEY lie la colonne 'CountryCode' à la colonne 'Code' de la table 'country'. Cela établit une relation entre une ville et le pays auquel elle appartient.

## Table countrylanguage :
***PRIMARY KEY (CountryCode, Language)*** : Cette contrainte combine les champs 'CountryCode' et 'Language' comme clé primaire de la table 'countrylanguage', garantissant ainsi l'unicité des combinaisons pays-langue.

***FOREIGN KEY (CountryCode) REFERENCES country (Code)*** : La contrainte FOREIGN KEY lie la colonne 'CountryCode' à la colonne 'Code' de la table 'country'. Cela établit une relation entre une langue et le pays auquel elle est associée.

Ces contraintes assurent l'intégrité des données et établissent des liens logiques entre les différentes entités (pays, villes, langues) de la base de données. Les clés étrangères assurent que les valeurs présentes dans certaines colonnes correspondent à des valeurs existantes dans d'autres tables, garantissant ainsi la cohérence des relations entre les données.

# Suite Exo

# La Carte du monde par région
![Carte du monde](newplot.png)

# use world;
# 1 Liste des type de gouvernement avec nombre de pays pour chaque.
SELECT GovernmentForm, COUNT(*) as Number_of_Countries
FROM country
GROUP BY GovernmentForm
ORDER BY Number_of_Countries DESC;

# Rep1 :

| GovernmentForm                               | Number_of_Countries |
|----------------------------------------------|---------------------|
| Republic                                     |                 123 |
| Constitutional Monarchy                      |                  29 |
| Federal Republic                             |                  14 |
| Dependent Territory of the UK                |                  12 |
| Monarchy                                     |                   5 |
| Territory of Australia                       |                   4 |
| Constitutional Monarchy, Federation          |                   4 |
| Overseas Department of France                |                   4 |
| Nonmetropolitan Territory of France          |                   4 |
| Socialistic Republic                         |                   3 |
| US Territory                                 |                   3 |
| Nonmetropolitan Territory of New Zealand     |                   3 |
| Nonmetropolitan Territory of The Netherlands |                   2 |
| Special Administrative Region of China       |                   2 |
| Commonwealth of the US                       |                   2 |
| Dependent Territory of Norway                |                   2 |
| Part of Denmark                              |                   2 |
| Monarchy (Sultanate)                         |                   2 |
| Islamic Republic                             |                   2 |
| Territorial Collectivity of France           |                   2 |
| Parliamentary Coprincipality                 |                   1 |
| Co-administrated                             |                   1 |
| Monarchy (Emirate)                           |                   1 |
| Dependent Territory of the US                |                   1 |
| Socialistic State                            |                   1 |
| Administrated by the UN                      |                   1 |
| People'sRepublic                             |                   1 |
| Constitutional Monarchy (Emirate)            |                   1 |
| Autonomous Area                              |                   1 |
| Parlementary Monarchy                        |                   1 |
| Islamic Emirate                              |                   1 |
| Emirate Federation                           |                   1 |
| Federation                                   |                   1 |
| Occupied by Marocco                          |                   1 |
| Independent Church State                     |                   1 |


# 2 Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?
Parce que l'utilisation d'un enum avec des valeurs spécifiques ('T' pour vrai et 'F' pour faux) permet d'exprimer plus explicitement le caractère officiel d'une langue. Cela peut être préférable dans le cas où, à l'avenir, d'autres états ou valeurs liées à la caractéristique d'officialité pourraient être introduits (par exemple, 'P' pour 
partiellement officiel, etc.).

En résumé, bien que dans certains cas un booléen puisse sembler plus simple pour 
représenter une vérité ou une fausse, l'utilisation d'un enum peut apporter plus de 
clarté et de flexibilité, en particulier si l'on envisage des extensions futures du 
modèle de données.

# 3 D’apres la BDD, combien de personne dans le monde parle anglais ?
SELECT SUM((c1.Percentage / 100) * c.Population) As TotalEnglishSpeakers
FROM countrylanguage c1
JOIN country c ON c1.CountryCode = c.Code
WHERE c1.Language = 'English';

# Rep3 :

| TotalEnglishSpeakers |
|----------------------|
|      347077867.30000 |


# 4 Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.
SELECT cl.Language, SUM((cl.Percentage / 100) * c.Population) AS TotalSpeakers
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
GROUP BY cl.Language
ORDER BY TotalSpeakers DESC;

# Rep4 :

| Language                  | TotalSpeakers    |
|---------------------------|------------------|
| Chinese                   | 1191843539.00000 |
| Hindi                     |  405633070.00000 |
| Spanish                   |  355029462.00000 |
| English                   |  347077867.30000 |
| Arabic                    |  233839238.70000 |
| Bengali                   |  209304719.00000 |
| Portuguese                |  177595269.40000 |
| Russian                   |  160807561.30000 |
| Japanese                  |  126814108.00000 |
| Punjabi                   |  104025371.00000 |
| German                    |   92133584.70000 |
| Javanese                  |   83570158.00000 |
| Telugu                    |   79065636.00000 |
| Marathi                   |   75019094.00000 |
| Korean                    |   72291372.00000 |
| Vietnamese                |   70616218.00000 |
| French                    |   69980880.40000 |
| Tamil                     |   68691536.00000 |
| Urdu                      |   63589470.00000 |
| Turkish                   |   62205657.20000 |
| Italian                   |   59864483.20000 |
| Gujarati                  |   48655776.00000 |
| Malay                     |   41517994.00000 |
| Kannada                   |   39532818.00000 |
| Polish                    |   39525035.10000 |
| Ukrainian                 |   36593408.00000 |
| Malajalam                 |   36491832.00000 |
| Thai                      |   33996960.00000 |
| Sunda                     |   33512906.00000 |
| Orija                     |   33450846.00000 |
| Pashto                    |   32404553.00000 |
| Burmese                   |   31471590.00000 |
| Persian                   |   31124734.00000 |
| Hausa                     |   29225396.00000 |
| Joruba                    |   24868874.00000 |
| Ful                       |   23704612.00000 |
| Romanian                  |   23457777.30000 |
| Uzbek                     |   22535760.00000 |
| Pilipino                  |   22258331.00000 |
| Dutch                     |   21388666.00000 |
| Ibo                       |   20182586.00000 |
| Lao                       |   20167307.00000 |
| Oromo                     |   19395150.00000 |
| Kurdish                   |   19062628.00000 |
| Azerbaijani               |   19014911.00000 |
| Amhara                    |   18769500.00000 |
| Sindhi                    |   18464994.00000 |
| Zhuang                    |   17885812.00000 |
| Cebuano                   |   17700311.00000 |
| Serbo-Croatian            |   16762504.70000 |
| Malagasy                  |   15800413.00000 |
| Asami                     |   15527778.00000 |
| Saraiki                   |   15335334.00000 |
| Akan                      |   15026888.00000 |
| Min                       |   14844752.00000 |
| Berberi                   |   13817820.00000 |
| Rwanda                    |   13750258.00000 |
| Nepali                    |   12799872.00000 |
| Somali                    |   12770598.00000 |
| Hungarian                 |   12652201.90000 |
| Khmer                     |   11810683.00000 |
| Greek                     |   11638803.60000 |
| Mantšu                    |   11498022.00000 |
| Kongo                     |   11480181.00000 |
| Singali                   |   11352681.00000 |
| Hui                       |   10220464.00000 |
| Shona                     |    9892055.00000 |
| Miao                      |    9661394.00000 |
| Zulu                      |    9508689.00000 |
| Luba                      |    9297720.00000 |
| Kazakh                    |    9258230.00000 |
| Mossi                     |    9185870.00000 |
| Madura                    |    9120601.00000 |
| Czech                     |    8362000.80000 |
| Swedish                   |    8255142.60000 |
| Haiti Creole              |    8222000.00000 |
| Belorussian               |    7671574.00000 |
| Yi                        |    7665348.00000 |
| Uighur                    |    7665348.00000 |
| Dari                      |    7293120.00000 |
| Mongolian                 |    7207888.00000 |
| Xhosa                     |    7146729.00000 |
| Nyamwesi                  |    7072087.00000 |
| Ilocano                   |    7064931.00000 |
| Bulgariana                |    7036276.80000 |
| Mongo                     |    6973290.00000 |
| Hiligaynon                |    6912997.00000 |
| Catalan                   |    6690841.30000 |
| Canton Chinese            |    6628268.00000 |
| Kirundi                   |    6567795.00000 |
| Balochi                   |    6456116.00000 |
| Tigrinja                  |    6395030.00000 |
| Tujia                     |    6387790.00000 |
| Chichewa                  |    6369275.00000 |
| Kikuyu                    |    6286720.00000 |
| Ibibio                    |    6244336.00000 |
| Afrikaans                 |    5937881.00000 |
| Ketšua                    |    5768437.00000 |
| Albaniana                 |    5664230.80000 |
| Tatar                     |    5525159.00000 |
| Makua                     |    5471040.00000 |
| Tibetan                   |    5110232.00000 |
| Minangkabau               |    5090568.00000 |
| Kanuri                    |    5043866.00000 |
| Slovak                    |    5024431.50000 |
| Finnish                   |    5016543.10000 |
| Danish                    |    5008464.00000 |
| Tadzhik                   |    4956520.00000 |
| Turkmenian                |    4934965.00000 |
| Wolof                     |    4901011.00000 |
| Ovimbundu                 |    4790616.00000 |
| Bugi                      |    4666354.00000 |
| Batakki                   |    4666354.00000 |
| Creole English            |    4518135.00000 |
| Tswana                    |    4495147.00000 |
| Mandarin Chinese          |    4479132.00000 |
| Malinke                   |    4459198.00000 |
| Norwegian                 |    4386528.00000 |
| Bicol                     |    4330119.00000 |
| Tsonga                    |    4176531.00000 |
| Luhya                     |    4151040.00000 |
| Hebrew                    |    4050068.00000 |
| Armenian                  |    4024652.00000 |
| Zande                     |    3947124.00000 |
| Ganda                     |    3941818.00000 |
| Shan                      |    3876935.00000 |
| Luo                       |    3850240.00000 |
| Banja                     |    3817926.00000 |
| Fang                      |    3794797.00000 |
| Papuan Languages          |    3792451.00000 |
| Hindko                    |    3755592.00000 |
| Mixed Languages           |    3690092.00000 |
| Edo                       |    3679698.00000 |
| Northsotho                |    3674307.00000 |
| Bali                      |    3605819.00000 |
| Gilaki                    |    3588206.00000 |
| Bambara                   |    3572412.00000 |
| Georgiana                 |    3562056.00000 |
| Lomwe                     |    3545240.00000 |
| Ewe                       |    3479156.00000 |
| Dinka                     |    3391350.00000 |
| Kamba                     |    3368960.00000 |
| Kalenjin                  |    3248640.00000 |
| Southsotho                |    3068652.00000 |
| Lithuanian                |    3047066.40000 |
| Ngala and Bangi           |    2995932.00000 |
| Swahili                   |    2949496.00000 |
| Gurage                    |    2940555.00000 |
| Luri                      |    2911186.00000 |
| Waray-waray               |    2886746.00000 |
| Maithili                  |    2847670.00000 |
| Karen                     |    2827882.00000 |
| Bamileke-bamum            |    2805810.00000 |
| Kirgiz                    |    2805303.00000 |
| Mbundu                    |    2781648.00000 |
| Creole French             |    2767307.00000 |
| Bemba                     |    2723193.00000 |
| Tiv                       |    2564638.00000 |
| Hakka                     |    2556672.00000 |
| Puyi                      |    2555116.00000 |
| Dong                      |    2555116.00000 |
| Galecian                  |    2524268.80000 |
| Arabic-French             |    2521118.00000 |
| Ndebele                   |    2517119.00000 |
| Mazandarani               |    2437272.00000 |
| Fon                       |    2426606.00000 |
| Nubian Languages          |    2388690.00000 |
| Sara                      |    2350687.00000 |
| Nkole                     |    2330246.00000 |
| Hehet                     |    2312673.00000 |
| Pampango                  |    2279010.00000 |
| Songhai-zerma             |    2274760.00000 |
| Guaraní                   |    2212225.00000 |
| Hassaniya                 |    2181390.00000 |
| Rakhine                   |    2052495.00000 |
| Bhojpuri                  |    2039088.00000 |
| Ijo                       |    2007108.00000 |
| Sidamo                    |    2002080.00000 |
| Makonde                   |    1977503.00000 |
| Haya                      |    1977503.00000 |
| Rundi                     |    1962852.00000 |
| Tamashek                  |    1936002.00000 |
| Swazi                     |    1915617.00000 |
| Teke                      |    1903797.00000 |
| Slovene                   |    1895003.40000 |
| Beja                      |    1887360.00000 |
| Brahui                    |    1877796.00000 |
| Sena                      |    1849920.00000 |
| Gusii                     |    1834880.00000 |
| Sotho                     |    1830050.00000 |
| Nyakusa                   |    1809918.00000 |
| Kiga                      |    1807574.00000 |
| Soga                      |    1785796.00000 |
| Bura                      |    1784096.00000 |
| Náhuatl                   |    1779858.00000 |
| Kru                       |    1779618.00000 |
| Walaita                   |    1751820.00000 |
| Gur                       |    1729962.00000 |
| Mende                     |    1689192.00000 |
| Meru                      |    1654400.00000 |
| Duala                     |    1644265.00000 |
| Luguru                    |    1642333.00000 |
| Chaga and Pare            |    1642333.00000 |
| Nyanja                    |    1621340.00000 |
| Macedonian                |    1615524.60000 |
| Ga-adangme                |    1576536.00000 |
| Sardinian                 |    1557360.00000 |
| Temne                     |    1543572.00000 |
| Gurma                     |    1504791.00000 |
| Chokwe                    |    1470648.00000 |
| Araucan                   |    1460256.00000 |
| Nuer                      |    1445010.00000 |
| Nyika                     |    1443840.00000 |
| Yao                       |    1442100.00000 |
| Shambala                  |    1441231.00000 |
| Malenasian Languages      |    1438620.00000 |
| Tho                       |    1436976.00000 |
| Pangasinan                |    1367406.00000 |
| Senufo and Minianka       |    1348080.00000 |
| Latvian                   |    1335734.20000 |
| Moravian                  |    1325874.90000 |
| Chuvash                   |    1322406.00000 |
| Gogo                      |    1307163.00000 |
| Teso                      |    1306680.00000 |
| Tharu                     |    1292220.00000 |
| Lango                     |    1284902.00000 |
| Soninke                   |    1271881.00000 |
| Southern Slavic Languages |    1265504.00000 |
| Tigre                     |    1220450.00000 |
| Muong                     |    1197480.00000 |
| Boa                       |    1188042.00000 |
| Serer                     |    1185125.00000 |
| Tswa                      |    1180800.00000 |
| Ha                        |    1173095.00000 |
| Tamang                    |    1172570.00000 |
| Bakhtyari                 |    1150934.00000 |
| Quiché                    |    1149885.00000 |
| [South]Mande              |    1138522.00000 |
| Chuabo                    |    1121760.00000 |
| Tikar                     |    1116290.00000 |
| Tagalog                   |    1113428.00000 |
| Mon                       |    1094664.00000 |
| Yucatec                   |    1087691.00000 |
| Maguindanao               |    1063538.00000 |
| Dzongkha                  |    1062000.00000 |
| Bashkir                   |    1028538.00000 |
| Lugbara                   |    1023566.00000 |
| Cakchiquel                |    1013265.00000 |
| Tonga                     |    1008590.00000 |
| Chin                      |    1003442.00000 |
| Crioulo                   |     996393.00000 |
| Maranao                   |     987571.00000 |
| Gisu                      |     980010.00000 |
| Acholi                    |     958232.00000 |
| Kpelle                    |     956810.00000 |
| Estonian                  |     950140.20000 |
| Romani                    |     943952.00000 |
| Aimará                    |     932809.00000 |
| Mon-khmer                 |     896445.00000 |
| Venda                     |     888294.00000 |
| Newari                    |     885410.00000 |
| Chechen                   |     881604.00000 |
| Mayo-kebbi                |     879865.00000 |
| Nung                      |     878152.00000 |
| Ovambo                    |     875082.00000 |
| Gbaya                     |     860370.00000 |
| Mandara                   |     859845.00000 |
| Banda                     |     849525.00000 |
| Susu                      |     817300.00000 |
| Songhai                   |     775146.00000 |
| Maka                      |     739165.00000 |
| Bari                      |     737250.00000 |
| Mordva                    |     734670.00000 |
| Ngoni                     |     731975.00000 |
| Ronga                     |     728160.00000 |
| Nyaneka-nkhumbi           |     695412.00000 |
| Luimbe-nganguela          |     695412.00000 |
| Friuli                    |     692160.00000 |
| Marendje                  |     688800.00000 |
| Kanem-bornu               |     688590.00000 |
| Adja                      |     676767.00000 |
| Kuy                       |     675389.00000 |
| Ouaddai                   |     665637.00000 |
| Kabyé                     |     638802.00000 |
| Kachin                    |     638554.00000 |
| Basque                    |     631067.20000 |
| Iban                      |     622832.00000 |
| Fur                       |     619290.00000 |
| Diola                     |     594110.00000 |
| Mixtec                    |     593286.00000 |
| Zapotec                   |     593286.00000 |
| Masana                    |     588315.00000 |
| Avarian                   |     587736.00000 |
| Mari                      |     587736.00000 |
| Fries                     |     586968.00000 |
| Lozi                      |     586816.00000 |
| Man                       |     558824.00000 |
| Kekchí                    |     557865.00000 |
| Kymri                     |     536610.60000 |
| Mandjia                   |     535020.00000 |
| Aizo                      |     530439.00000 |
| Bariba                    |     530439.00000 |
| Chewa                     |     522633.00000 |
| Chakma                    |     516620.00000 |
| Hadjarai                  |     512617.00000 |
| Chilluk                   |     501330.00000 |
| Tandjile                  |     497315.00000 |
| Karakalpak                |     486360.00000 |
| Masai                     |     481280.00000 |
| Watyi                     |     476787.00000 |
| Gorane                    |     474362.00000 |
| Luvale                    |     463608.00000 |
| Indian Languages          |     454765.00000 |
| Comorian                  |     453072.00000 |
| Kissi                     |     445800.00000 |
| Lotuko                    |     442350.00000 |
| Udmur                     |     440802.00000 |
| Bassa                     |     432098.00000 |
| Turkana                   |     421120.00000 |
| Busansi                   |     417795.00000 |
| Fijian                    |     415036.00000 |
| Somba                     |     408499.00000 |
| Limba                     |     402882.00000 |
| Otomí                     |     395524.00000 |
| Nsenga                    |     394267.00000 |
| Afar                      |     387574.00000 |
| Yalunka                   |     380506.00000 |
| Dagara                    |     370047.00000 |
| Maltese                   |     364231.60000 |
| Loma                      |     353822.00000 |
| Sranantonga               |     337770.00000 |
| Mboshi                    |     335502.00000 |
| Mbete                     |     310452.00000 |
| Dyula                     |     310362.00000 |
| Ambo                      |     309072.00000 |
| Luchazi                   |     309072.00000 |
| Mam                       |     307395.00000 |
| Arabic-French-English     |     306752.00000 |
| Dhivehi                   |     286000.00000 |
| Lao-Soung                 |     282516.00000 |
| Grebo                     |     280706.00000 |
| Luxembourgish             |     280590.80000 |
| Ngbaka                    |     271125.00000 |
| Tšam                      |     268032.00000 |
| Icelandic                 |     267003.00000 |
| Papiamento                |     266055.00000 |
| Ane                       |     263853.00000 |
| Kotokoli                  |     263853.00000 |
| Marma                     |     258310.00000 |
| Bajan                     |     256770.00000 |
| Moba                      |     249966.00000 |
| Gio                       |     249166.00000 |
| Kono-vai                  |     247554.00000 |
| Dusun                     |     244684.00000 |
| Mbum                      |     231360.00000 |
| Mano                      |     227088.00000 |
| Nama                      |     214024.00000 |
| Punu-sira-nzebi           |     209646.00000 |
| Naudemba                  |     189789.00000 |
| Philippene Languages      |     188156.00000 |
| Assyrian                  |     184920.00000 |
| Bullom-sherbro            |     184452.00000 |
| Kayah                     |     182444.00000 |
| Chibcha                   |     181353.00000 |
| Mpongwe                   |     178996.00000 |
| Lezgian                   |     177882.00000 |
| Balante                   |     177098.00000 |
| Kavango                   |     167422.00000 |
| Maori                     |     166066.00000 |
| Kuranko                   |     165036.00000 |
| Guaymí                    |     151368.00000 |
| Samoan                    |     147108.00000 |
| Hadareb                   |     146300.00000 |
| Tukulor                   |     144180.00000 |
| Gagauzi                   |     140160.00000 |
| Herero                    |     138080.00000 |
| Ami                       |     133536.00000 |
| Khasi                     |     129155.00000 |
| Santhali                  |     129155.00000 |
| Garo                      |     129155.00000 |
| Tripuri                   |     129155.00000 |
| Fukien                    |     128858.00000 |
| Osseetti                  |     119232.00000 |
| Saho                      |     115500.00000 |
| Bilin                     |     115500.00000 |
| Tahitian                  |     109040.00000 |
| Bislama                   |     107540.00000 |
| Garifuna                  |     100693.00000 |
| Tongan                    |      99425.00000 |
| Goajiro                   |      96680.00000 |
| Chiu chau                 |      94948.00000 |
| Malay-English             |      94464.00000 |
| Miskito                   |      94154.00000 |
| Samoan-English            |      93600.00000 |
| San                       |      89564.00000 |
| Atayal                    |      89024.00000 |
| Punu                      |      85347.00000 |
| Kiribati                  |      85135.00000 |
| Abhyasi                   |      84456.00000 |
| Caprivi                   |      81122.00000 |
| Sango                     |      76518.00000 |
| Comorian-French           |      74562.00000 |
| Chamorro                  |      73128.00000 |
| Dorbet                    |      71874.00000 |
| Seselwa                   |      70301.00000 |
| Paiwan                    |      66768.00000 |
| Mahoré                    |      62431.00000 |
| Marshallese               |      61952.00000 |
| Caribbean                 |      61263.00000 |
| Irish                     |      60401.60000 |
| Gaeli                     |      59623.40000 |
| Mandyako                  |      59437.00000 |
| Czech and Moravian        |      59385.70000 |
| Cuna                      |      57120.00000 |
| Arawakan                  |      54375.00000 |
| Circassian                |      50830.00000 |
| Bajad                     |      50578.00000 |
| Trukese                   |      49504.00000 |
| Greenlandic               |      49000.00000 |
| Buryat                    |      45254.00000 |
| Faroese                   |      43000.00000 |
| Romansh                   |      42962.40000 |
| Polynesian Languages      |      41696.00000 |
| Silesiana                 |      41112.40000 |
| Khoekhoe                  |      40550.00000 |
| Bubi                      |      39411.00000 |
| Dariganga                 |      37268.00000 |
| Ukrainian and Russian     |      32392.20000 |
| Zenaga                    |      32040.00000 |
| Comorian-madagassi        |      31790.00000 |
| Eskimo Languages          |      31147.00000 |
| Rapa nui                  |      30422.00000 |
| Pohnpei                   |      28322.00000 |
| Warrau                    |      24170.00000 |
| Maya Languages            |      23136.00000 |
| Embera                    |      17136.00000 |
| Palau                     |      15618.00000 |
| Tuvalu                    |      12535.00000 |
| Sumo                      |      10148.00000 |
| Comorian-Arabic           |       9248.00000 |
| Mortlock                  |       9044.00000 |
| Kosrean                   |       8687.00000 |
| Yap                       |       6902.00000 |
| Nauru                     |       6900.00000 |
| Monegasque                |       5474.00000 |
| Wolea                     |       4403.00000 |
| Carolinian                |       3744.00000 |
| Comorian-Swahili          |       2890.00000 |
| Wallis                    |          0.00000 |
| Nahua                     |          0.00000 |
| Soqutri                   |          0.00000 |
| Sinaberberi               |          0.00000 |
| Futuna                    |          0.00000 |
| Ainu                      |          0.00000 |
| Saame                     |          0.00000 |
| Tokelau                   |          0.00000 |
| Pitcairnese               |          0.00000 |
| Niue                      |          0.00000 |

# 5 En quelle unité est exprimée la surface des pays ?
La surface des pays est exprimée en Kilomètres carrés (km²)

# 6 Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale
SELECT
    c.Name AS Country,
    c.Capital AS Capital,
    c.Population AS TotalPop,
    city.Name AS CapitalName,
    (city.Population / c.Population) * 100 AS CapPopPercent
FROM country c
JOIN city ON c.Capital = city.ID
WHERE c.Population > 10000000
ORDER BY c.Population DESC;

# Rep6 :

| Country                               | Capital | TotalPop   | CapitalName         | CapPopPercent |
|---------------------------------------|---------|------------|---------------------|---------------|
| China                                 |    1891 | 1277558000 | Peking              |        0.5849 |
| India                                 |    1109 | 1013662000 | New Delhi           |        0.0297 |
| United States                         |    3813 |  278357000 | Washington          |        0.2055 |
| Indonesia                             |     939 |  212107000 | Jakarta             |        4.5283 |
| Brazil                                |     211 |  170115000 | Brasília            |        1.1580 |
| Pakistan                              |    2831 |  156483000 | Islamabad           |        0.3352 |
| Russian Federation                    |    3580 |  146934000 | Moscow              |        5.7095 |
| Bangladesh                            |     150 |  129155000 | Dhaka               |        2.7973 |
| Japan                                 |    1532 |  126714000 | Tokyo               |        6.2978 |
| Nigeria                               |    2754 |  111506000 | Abuja               |        0.3140 |
| Mexico                                |    2515 |   98881000 | Ciudad de México    |        8.6885 |
| Germany                               |    3068 |   82164700 | Berlin              |        4.1218 |
| Vietnam                               |    3770 |   79832000 | Hanoi               |        1.7662 |
| Philippines                           |     766 |   75967000 | Manila              |        2.0813 |
| Egypt                                 |     608 |   68470000 | Cairo               |        9.9160 |
| Iran                                  |    1380 |   67702000 | Teheran             |        9.9832 |
| Turkey                                |    3358 |   66591000 | Ankara              |        4.5624 |
| Ethiopia                              |     756 |   62565000 | Addis Abeba         |        3.9879 |
| Thailand                              |    3320 |   61399000 | Bangkok             |       10.2936 |
| United Kingdom                        |     456 |   59623400 | London              |       12.2184 |
| France                                |    2974 |   59225700 | Paris               |        3.5884 |
| Italy                                 |    1464 |   57680000 | Roma                |        4.5832 |
| Congo, The Democratic Republic of the |    2298 |   51654000 | Kinshasa            |        9.8037 |
| Ukraine                               |    3426 |   50456000 | Kyiv                |        5.2006 |
| South Korea                           |    2331 |   46844000 | Seoul               |       21.3082 |
| Myanmar                               |    2710 |   45611000 | Rangoon (Yangon)    |        7.3704 |
| Colombia                              |    2257 |   42321000 | Santafé de Bogotá   |       14.7937 |
| South Africa                          |     716 |   40377000 | Pretoria            |        1.6312 |
| Spain                                 |     653 |   39441700 | Madrid              |        7.2995 |
| Poland                                |    2928 |   38653600 | Warszawa            |        4.1791 |
| Argentina                             |      69 |   37032000 | Buenos Aires        |        8.0529 |
| Tanzania                              |    3306 |   33517000 | Dodoma              |        0.5639 |
| Algeria                               |      35 |   31471000 | Alger               |        6.8889 |
| Canada                                |    1822 |   31147000 | Ottawa              |        1.0764 |
| Kenya                                 |    1881 |   30080000 | Nairobi             |        7.6130 |
| Sudan                                 |    3225 |   29490000 | Khartum             |        3.2129 |
| Morocco                               |    2486 |   28351000 | Rabat               |        2.1991 |
| Peru                                  |    2890 |   25662000 | Lima                |       25.1917 |
| Uzbekistan                            |    3503 |   24318000 | Toskent             |        8.7075 |
| Venezuela                             |    3539 |   24170000 | Caracas             |        8.1725 |
| North Korea                           |    2318 |   24039000 | Pyongyang           |       10.3332 |
| Nepal                                 |    2729 |   23930000 | Kathmandu           |        2.4732 |
| Iraq                                  |    1365 |   23115000 | Baghdad             |       18.7584 |
| Afghanistan                           |       1 |   22720000 | Kabul               |        7.8345 |
| Romania                               |    3018 |   22455500 | Bucuresti           |        8.9783 |
| Taiwan                                |    3263 |   22256000 | Taipei              |       11.8679 |
| Malaysia                              |    2464 |   22244000 | Kuala Lumpur        |        5.8332 |
| Uganda                                |    3425 |   21778000 | Kampala             |        4.0904 |
| Saudi Arabia                          |    3173 |   21607000 | Riyadh              |       15.3839 |
| Ghana                                 |     910 |   20212000 | Accra               |        5.2939 |
| Mozambique                            |    2698 |   19680000 | Maputo              |        5.1775 |
| Australia                             |     135 |   18886000 | Canberra            |        1.7088 |
| Sri Lanka                             |    3217 |   18827000 | Colombo             |        3.4259 |
| Yemen                                 |    1780 |   18112000 | Sanaa               |        2.7805 |
| Kazakstan                             |    1864 |   16223000 | Astana              |        1.9183 |
| Syria                                 |    3250 |   16125000 | Damascus            |        8.3535 |
| Madagascar                            |    2455 |   15942000 | Antananarivo        |        4.2383 |
| Netherlands                           |       5 |   15864000 | Amsterdam           |        4.6092 |
| Chile                                 |     554 |   15211000 | Santiago de Chile   |       30.9247 |
| Cameroon                              |    1804 |   15085000 | Yaoundé             |        9.1004 |
| Côte d’Ivoire                         |    2814 |   14786000 | Yamoussoukro        |        0.8792 |
| Angola                                |      56 |   12878000 | Luanda              |       15.7012 |
| Ecuador                               |     594 |   12646000 | Quito               |       12.4423 |
| Burkina Faso                          |     549 |   11937000 | Ouagadougou         |        6.9029 |
| Zimbabwe                              |    4068 |   11669000 | Harare              |       12.0833 |
| Guatemala                             |     922 |   11385000 | Ciudad de Guatemala |        7.2315 |
| Mali                                  |    2482 |   11234000 | Bamako              |        7.2063 |
| Cuba                                  |    2413 |   11201000 | La Habana           |       20.1411 |
| Cambodia                              |    1800 |   11168000 | Phnom Penh          |        5.1053 |
| Malawi                                |    2462 |   10925000 | Lilongwe            |        3.9905 |
| Niger                                 |    2738 |   10730000 | Niamey              |        3.9143 |
| Yugoslavia                            |    1792 |   10640000 | Beograd             |       11.3158 |
| Greece                                |    2401 |   10545700 | Athenai             |        7.3212 |
| Czech Republic                        |    3339 |   10278100 | Praha               |       11.4917 |
| Belgium                               |     179 |   10239000 | Bruxelles [Brussel] |        1.3073 |
| Belarus                               |    3520 |   10236000 | Minsk               |       16.3540 |
| Somalia                               |    3214 |   10097000 | Mogadishu           |        9.8742 |
| Hungary                               |    3483 |   10043200 | Budapest            |       18.0376 |

# 7 Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance
SELECT c.Name AS Country,
       c.GNP AS GNP_n,
       c.GNPOld AS GNPOld_n_minus_1,
       ((c.GNP - c.GNPOld) / c.GNPOld) * 100 AS GrowthPercentage
FROM country c
ORDER BY GrowthPercentage DESC
LIMIT 10;

# Rep7 :

| Country                               | GNP_n     | GNPOld_n_minus_1 | GrowthPercentage |
|---------------------------------------|-----------|------------------|------------------|
| Congo, The Democratic Republic of the |   6964.00 |          2474.00 |       181.487470 |
| Turkmenistan                          |   4397.00 |          2000.00 |       119.850000 |
| Tajikistan                            |   1990.00 |          1056.00 |        88.446970 |
| Estonia                               |   5328.00 |          3371.00 |        58.053990 |
| Albania                               |   3205.00 |          2500.00 |        28.200000 |
| Suriname                              |    870.00 |           706.00 |        23.229462 |
| Iran                                  | 195746.00 |        160151.00 |        22.225899 |
| Bulgaria                              |  12178.00 |         10169.00 |        19.756122 |
| Honduras                              |   5333.00 |          4697.00 |        13.540558 |
| Latvia                                |   6398.00 |          5639.00 |        13.459833 |

# 8 Liste des pays plurilingues avec pour chacun le nombre de langues parlées.
SELECT c.Name AS Country,COUNT(cl.Language) AS NumberOfLanguages
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Code, c.Name
HAVING COUNT(cl.Language) > 1
ORDER BY NumberOfLanguages DESC;

# Rep8 :

| Country                               | NumberOfLanguages |
|---------------------------------------|-------------------|
| Canada                                |                12 |
| Russian Federation                    |                12 |
| India                                 |                12 |
| United States                         |                12 |
| China                                 |                12 |
| South Africa                          |                11 |
| Tanzania                              |                11 |
| Iran                                  |                10 |
| Kenya                                 |                10 |
| Mozambique                            |                10 |
| Congo, The Democratic Republic of the |                10 |
| Nigeria                               |                10 |
| Uganda                                |                10 |
| Philippines                           |                10 |
| Sudan                                 |                10 |
| Angola                                |                 9 |
| Vietnam                               |                 9 |
| Indonesia                             |                 9 |
| Namibia                               |                 8 |
| Pakistan                              |                 8 |
| Austria                               |                 8 |
| Australia                             |                 8 |
| Sierra Leone                          |                 8 |
| Togo                                  |                 8 |
| Cameroon                              |                 8 |
| Myanmar                               |                 8 |
| Chad                                  |                 8 |
| Italy                                 |                 8 |
| Czech Republic                        |                 8 |
| Liberia                               |                 8 |
| Ethiopia                              |                 7 |
| Benin                                 |                 7 |
| Guinea                                |                 7 |
| Kyrgyzstan                            |                 7 |
| Nepal                                 |                 7 |
| Denmark                               |                 7 |
| Ukraine                               |                 7 |
| Bangladesh                            |                 7 |
| Mexico                                |                 6 |
| Sweden                                |                 6 |
| Burkina Faso                          |                 6 |
| Central African Republic              |                 6 |
| Guinea-Bissau                         |                 6 |
| Malaysia                              |                 6 |
| Panama                                |                 6 |
| Belgium                               |                 6 |
| Romania                               |                 6 |
| Mauritius                             |                 6 |
| Eritrea                               |                 6 |
| Hungary                               |                 6 |
| Ghana                                 |                 6 |
| Latvia                                |                 6 |
| Mauritania                            |                 6 |
| Georgia                               |                 6 |
| Uzbekistan                            |                 6 |
| Congo                                 |                 6 |
| Kazakstan                             |                 6 |
| Northern Mariana Islands              |                 6 |
| Thailand                              |                 6 |
| Japan                                 |                 6 |
| Mongolia                              |                 6 |
| Micronesia, Federated States of       |                 6 |
| Zambia                                |                 6 |
| Germany                               |                 6 |
| France                                |                 6 |
| Mali                                  |                 6 |
| Yugoslavia                            |                 6 |
| Senegal                               |                 6 |
| Taiwan                                |                 6 |
| Finland                               |                 5 |
| Iraq                                  |                 5 |
| Afghanistan                           |                 5 |
| Estonia                               |                 5 |
| Slovakia                              |                 5 |
| Botswana                              |                 5 |
| Gambia                                |                 5 |
| Moldova                               |                 5 |
| Réunion                               |                 5 |
| Comoros                               |                 5 |
| Nauru                                 |                 5 |
| Brazil                                |                 5 |
| Colombia                              |                 5 |
| Luxembourg                            |                 5 |
| Norway                                |                 5 |
| Hong Kong                             |                 5 |
| Lithuania                             |                 5 |
| Guam                                  |                 5 |
| Côte d’Ivoire                         |                 5 |
| Guatemala                             |                 5 |
| Macedonia                             |                 5 |
| Niger                                 |                 5 |
| Laos                                  |                 4 |
| Aruba                                 |                 4 |
| Costa Rica                            |                 4 |
| Spain                                 |                 4 |
| Malawi                                |                 4 |
| Monaco                                |                 4 |
| Azerbaijan                            |                 4 |
| Brunei                                |                 4 |
| Cambodia                              |                 4 |
| Macao                                 |                 4 |
| Turkmenistan                          |                 4 |
| Bolivia                               |                 4 |
| Honduras                              |                 4 |
| Paraguay                              |                 4 |
| Netherlands                           |                 4 |
| Belize                                |                 4 |
| Gabon                                 |                 4 |
| Zimbabwe                              |                 4 |
| Belarus                               |                 4 |
| Nicaragua                             |                 4 |
| Poland                                |                 4 |
| Andorra                               |                 4 |
| Chile                                 |                 4 |
| Palau                                 |                 4 |
| Bulgaria                              |                 4 |
| Switzerland                           |                 4 |
| Lebanon                               |                 3 |
| Peru                                  |                 3 |
| Tuvalu                                |                 3 |
| Mayotte                               |                 3 |
| Slovenia                              |                 3 |
| Turkey                                |                 3 |
| Vanuatu                               |                 3 |
| Tunisia                               |                 3 |
| Trinidad and Tobago                   |                 3 |
| Virgin Islands, U.S.                  |                 3 |
| Burundi                               |                 3 |
| Bhutan                                |                 3 |
| Venezuela                             |                 3 |
| French Polynesia                      |                 3 |
| Tajikistan                            |                 3 |
| American Samoa                        |                 3 |
| United Kingdom                        |                 3 |
| Guyana                                |                 3 |
| Lesotho                               |                 3 |
| Solomon Islands                       |                 3 |
| Argentina                             |                 3 |
| Jordan                                |                 3 |
| Sri Lanka                             |                 3 |
| Djibouti                              |                 3 |
| Liechtenstein                         |                 3 |
| Netherlands Antilles                  |                 3 |
| Singapore                             |                 3 |
| Israel                                |                 3 |
| Seychelles                            |                 3 |
| Albania                               |                 3 |
| New Caledonia                         |                 3 |
| Samoa                                 |                 3 |
| Greece                                |                 2 |
| Wallis and Futuna                     |                 2 |
| Christmas Island                      |                 2 |
| Equatorial Guinea                     |                 2 |
| Maldives                              |                 2 |
| Rwanda                                |                 2 |
| Ireland                               |                 2 |
| Kuwait                                |                 2 |
| Madagascar                            |                 2 |
| South Korea                           |                 2 |
| Suriname                              |                 2 |
| Cape Verde                            |                 2 |
| Guadeloupe                            |                 2 |
| Saint Kitts and Nevis                 |                 2 |
| Oman                                  |                 2 |
| Sao Tome and Principe                 |                 2 |
| Tonga                                 |                 2 |
| Kiribati                              |                 2 |
| Morocco                               |                 2 |
| Martinique                            |                 2 |
| New Zealand                           |                 2 |
| Qatar                                 |                 2 |
| East Timor                            |                 2 |
| Barbados                              |                 2 |
| Egypt                                 |                 2 |
| Gibraltar                             |                 2 |
| Haiti                                 |                 2 |
| Somalia                               |                 2 |
| Saint Vincent and the Grenadines      |                 2 |
| Ecuador                               |                 2 |
| Croatia                               |                 2 |
| Palestine                             |                 2 |
| Tokelau                               |                 2 |
| Antigua and Barbuda                   |                 2 |
| Cook Islands                          |                 2 |
| Algeria                               |                 2 |
| El Salvador                           |                 2 |
| Dominican Republic                    |                 2 |
| Armenia                               |                 2 |
| Niue                                  |                 2 |
| North Korea                           |                 2 |
| Dominica                              |                 2 |
| Puerto Rico                           |                 2 |
| Svalbard and Jan Mayen                |                 2 |
| United Arab Emirates                  |                 2 |
| Faroe Islands                         |                 2 |
| French Guiana                         |                 2 |
| Jamaica                               |                 2 |
| Malta                                 |                 2 |
| Bahamas                               |                 2 |
| Saint Lucia                           |                 2 |
| Papua New Guinea                      |                 2 |
| Syria                                 |                 2 |
| Bahrain                               |                 2 |
| Greenland                             |                 2 |
| Libyan Arab Jamahiriya                |                 2 |
| Yemen                                 |                 2 |
| Cyprus                                |                 2 |
| Fiji Islands                          |                 2 |
| Iceland                               |                 2 |
| Marshall Islands                      |                 2 |
| Swaziland                             |                 2 |
| Cocos (Keeling) Islands               |                 2 |

# 9 Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.
SELECT
    c.Name AS Country,
    COUNT(DISTINCT cl.Language) AS TotLang,
    COUNT(DISTINCT CASE WHEN cl.IsOfficial = 'T' THEN cl.Language END) AS OffLang,
    GROUP_CONCAT(DISTINCT cl.Language ORDER BY cl.Language SEPARATOR ', ') AS AllLang
FROM country c
JOIN countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY c.Code, c.Name
HAVING OffLang > 1
ORDER BY OffLang DESC, TotLang DESC;

# Rep9 :

| Country              | TotLang | OffLang | AllLang                                                                                                             |
|----------------------|---------|---------|---------------------------------------------------------------------------------------------------------------------|
| South Africa         |      11 |       4 | Afrikaans, English, Ndebele, Northsotho, Southsotho, Swazi, Tsonga, Tswana, Venda, Xhosa, Zulu                      |
| Switzerland          |       4 |       4 | French, German, Italian, Romansh                                                                                    |
| Belgium              |       6 |       3 | Arabic, Dutch, French, German, Italian, Turkish                                                                     |
| Luxembourg           |       5 |       3 | French, German, Italian, Luxembourgish, Portuguese                                                                  |
| Bolivia              |       4 |       3 | Aimará, Guaraní, Ketšua, Spanish                                                                                    |
| Peru                 |       3 |       3 | Aimará, Ketšua, Spanish                                                                                             |
| Singapore            |       3 |       3 | Chinese, Malay, Tamil                                                                                               |
| Vanuatu              |       3 |       3 | Bislama, English, French                                                                                            |
| Canada               |      12 |       2 | Chinese, Dutch, English, Eskimo Languages, French, German, Italian, Polish, Portuguese, Punjabi, Spanish, Ukrainian |
| Togo                 |       8 |       2 | Ane, Ewe, Gurma, Kabyé, Kotokoli, Moba, Naudemba, Watyi                                                             |
| Kyrgyzstan           |       7 |       2 | Kazakh, Kirgiz, Russian, Tadzhik, Tatar, Ukrainian, Uzbek                                                           |
| Romania              |       6 |       2 | German, Hungarian, Romani, Romanian, Serbo-Croatian, Ukrainian                                                      |
| Afghanistan          |       5 |       2 | Balochi, Dari, Pashto, Turkmenian, Uzbek                                                                            |
| Finland              |       5 |       2 | Estonian, Finnish, Russian, Saame, Swedish                                                                          |
| Guam                 |       5 |       2 | Chamorro, English, Japanese, Korean, Philippene Languages                                                           |
| Nauru                |       5 |       2 | Chinese, English, Kiribati, Nauru, Tuvalu                                                                           |
| Belarus              |       4 |       2 | Belorussian, Polish, Russian, Ukrainian                                                                             |
| Palau                |       4 |       2 | Chinese, English, Palau, Philippene Languages                                                                       |
| Paraguay             |       4 |       2 | German, Guaraní, Portuguese, Spanish                                                                                |
| Netherlands Antilles |       3 |       2 | Dutch, English, Papiamento                                                                                          |
| American Samoa       |       3 |       2 | English, Samoan, Tongan                                                                                             |
| Burundi              |       3 |       2 | French, Kirundi, Swahili                                                                                            |
| Israel               |       3 |       2 | Arabic, Hebrew, Russian                                                                                             |
| Sri Lanka            |       3 |       2 | Mixed Languages, Singali, Tamil                                                                                     |
| Lesotho              |       3 |       2 | English, Sotho, Zulu                                                                                                |
| Seychelles           |       3 |       2 | English, French, Seselwa                                                                                            |
| Tuvalu               |       3 |       2 | English, Kiribati, Tuvalu                                                                                           |
| Samoa                |       3 |       2 | English, Samoan, Samoan-English                                                                                     |
| Cyprus               |       2 |       2 | Greek, Turkish                                                                                                      |
| Faroe Islands        |       2 |       2 | Danish, Faroese                                                                                                     |
| Greenland            |       2 |       2 | Danish, Greenlandic                                                                                                 |
| Ireland              |       2 |       2 | English, Irish                                                                                                      |
| Madagascar           |       2 |       2 | French, Malagasy                                                                                                    |
| Marshall Islands     |       2 |       2 | English, Marshallese                                                                                                |
| Malta                |       2 |       2 | English, Maltese                                                                                                    |
| Rwanda               |       2 |       2 | French, Rwanda                                                                                                      |
| Somalia              |       2 |       2 | Arabic, Somali                                                                                                      |
| Tonga                |       2 |       2 | English, Tongan                                                                                                     |

# 10 Liste des langues parlées en France avec le % pour chacune.
SELECT cl.Language AS Language, cl.Percentage AS Percentage
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
WHERE c.Name = 'France';

# Rep10 :

| Language   | Percentage |
|------------|------------|
| Arabic     |        2.5 |
| French     |       93.6 |
| Italian    |        0.4 |
| Portuguese |        1.2 |
| Spanish    |        0.4 |
| Turkish    |        0.4 |

# 11 Pareil en chine.
SELECT cl.Language AS Language, cl.Percentage AS Percentage
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
WHERE c.Name = 'China';

# Rep11 :

| Language  | Percentage |
|-----------|------------|
| Chinese   |       92.0 |
| Dong      |        0.2 |
| Hui       |        0.8 |
| Mantšu    |        0.9 |
| Miao      |        0.7 |
| Mongolian |        0.4 |
| Puyi      |        0.2 |
| Tibetan   |        0.4 |
| Tujia     |        0.5 |
| Uighur    |        0.6 |
| Yi        |        0.6 |
| Zhuang    |        1.4 |

# 12 Pareil aux états unis.
SELECT cl.Language AS Language, cl.Percentage AS Percentage
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
WHERE c.Name = 'United States';

# Rep12 :

| Language   | Percentage |
|------------|------------|
| Chinese    |        0.6 |
| English    |       86.2 |
| French     |        0.7 |
| German     |        0.7 |
| Italian    |        0.6 |
| Japanese   |        0.2 |
| Korean     |        0.3 |
| Polish     |        0.3 |
| Portuguese |        0.2 |
| Spanish    |        7.5 |
| Tagalog    |        0.4 |
| Vietnamese |        0.2 |

# 13 Pareil aux UK.
SELECT cl.Language AS Language, cl.Percentage AS Percentage
FROM countrylanguage cl
JOIN country c ON cl.CountryCode = c.Code
WHERE c.Name = 'United Kingdom';

# Rep13 :

| Language | Percentage |
|----------|------------|
| English  |       97.3 |
| Gaeli    |        0.1 |
| Kymri    |        0.9 |

# 15 Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?
/*Non, parce que La base de données peut ne pas contenir toutes les langues 
parlées dans un pays, et donc les pourcentages répertoriés peuvent ne représenter 
qu'une partie des langues. D'autres langues non répertoriées ne contribueraient 
pas à la somme totale des pourcentages.*/











