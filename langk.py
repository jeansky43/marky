import pandas as pd
import mysql.connector

# Connexion à la base de données classicmodelsV2
conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="classicmodelsV2"
)
curse = conn.cursor()

# Connexion à la base de données world
conn1 = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="world"
)
curse1 = conn1.cursor() 

# Exécuter la requête SQL
query = """
    SELECT dif.Code AS CountryL, dif.Language AS Lang
    FROM (
        SELECT 
            c.Code, 
            cl.Language, 
            cl.Percentage,
            ROW_NUMBER() OVER (PARTITION BY c.Code ORDER BY cl.Percentage DESC) AS Noth
        FROM world.country c
        JOIN world.countrylanguage cl ON cl.CountryCode = c.Code
        WHERE cl.IsOfficial = 'T'
    ) AS dif
    WHERE dif.Noth = 1;   
"""
df_1 = pd.read_sql(query, conn)


curse.close()
conn.close()

df = pd.read_csv('/home/user/Projects/marky/language-codes_csv.csv')
df["English"] = df["English"].str.split(";")
df = df.explode("English")
languages = df_1.merge(df, left_on='Lang', right_on='English')
country_language = dict(zip(languages['CountryL'], languages['alpha2']))
country_language["PHL"] = "tl"

# Connexion à la base de données classicmodelsV2 
with mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="classicmodelsV2"
) as conn:
    curse = conn.cursor()
    for country_code, language_code in country_language.items():
        update_query = """
        UPDATE customers
        SET Lang = %s
        WHERE country = %s
        """
        curse.execute(update_query, (language_code, country_code))
    conn.commit()

# Créer un DataFrame final
result_df = pd.DataFrame(list(country_language.items()), columns=['Country', 'Language'])
